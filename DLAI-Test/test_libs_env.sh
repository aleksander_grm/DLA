#!/bin/bash
#
# author: aleksander.grm@fpp.uni-lj.si
# date: 19/01/2021

# ***************************************
# *** Test libs for Prace-DLA project ***
# ***************************************

# Needed modules to be loaded
#
module purge
module load foss
module load Boost
module load CMake

# Module info
module list

# Set Intel-MKL environment
source /opt/intel/compilers_and_libraries/linux/mkl/bin/mklvars.sh intel64

# Parameters
#
rpath=$(pwd) # set local path for the installation

# Pull DLA-Interface
rm -rf ${rpath}/dlai
git clone https://github.com/eth-cscs/DLA-interface.git dlai # Clone remote git
cd dlai

# Start to check libs (build block)
mkdir build
cd build

echo "Build path: $(pwd)"

cmake ../ \
	-DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DDLA_LAPACK_TYPE="MKL" \
	-DMKL_THREADING="Sequential" \
	-DDLA_SCALAPACK_TYPE="MKL" 

# clean test dir
#rm -rf ${rpath}/dlai

#!/bin/bash

# Install DLA-Interface
rpath=$1
nproc=$2

echo "rpath: ${rpath}"


# ******************
# *** DLA-Future ***
# ******************

TM=DLA-Future
BLASPP_PATH=${rpath}/../${TM}/blaspp
LAPACKPP_PATH=${rpath}/../${TM}/lapackpp
HPX_PATH=${rpath}/../${TM}/hpx
DLAF_PATH=${rpath}/../${TM}/dlaf

echo "BLASPP_PATH: ${BLASPP_PATH}"
echo "LAPACKPP_PATH: ${LAPACKPP_PATH}"
echo "HPX_PATH: ${HPX_PATH}"
echo "DLAF_PATH: ${DLAF_PATH}"

export blaspp_DIR=${BLASPP_PATH}
export lapackpp_DIR=${LAPACKPP_PATH}
export HPX_DIR=${HPX_PATH}

# *****************


#DLAI_VER=unilj_fs                                                                                                     
DLAI_VER=

DLAI_PATH=${rpath}/dlai
DLAI_SOURCE_PATH=${rpath}/../DLA-Interface

rm -rf ${DLAI_PATH}
mkdir ${DLAI_PATH}

rm -rf build
mkdir build
cd build

echo
echo "Build path: $(pwd)"
echo "DLAI-src path: ${DLAI_SOURCE_PATH}"
echo
#
# Change ScaLAPACK define parameters

cmake ${DLAI_SOURCE_PATH} --log-level=DEBUG \
	-DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DMKL_INCLUDE_DIR=/usr/include/x86_64-linux-gnu \
	-DDLA_LAPACK_TYPE="MKL" \
	-DMKL_THREADING="Sequential" \
	-DDLA_SCALAPACK_TYPE="MKL" \
	-DMKL_MPI_TYPE="OpenMPI" \
	-DDLAFROOT=${DLAF_PATH} \
	-DCMAKE_INSTALL_PREFIX=${DLAI_PATH}

#make -j${nproc}
#make install

# Test DLA-Interface
#
#echo
#read -t 2 -p " Starting with DLA-Interface test ..."
#echo
#make test

# remove sources
#rm -rf ${rpath}/interface/dlaf-${DLAI_VER}

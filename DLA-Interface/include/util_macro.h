//
// Distributed Linear Algebra Interface (DLAI)
//
// Copyright (c) 2018-2019, ETH Zurich
// All rights reserved.
//
// Please, refer to the LICENSE file in the root directory.
// SPDX-License-Identifier: BSD-3-Clause
//

#ifndef DLA_INTERFACE_UTIL_MACRO_H
#define DLA_INTERFACE_UTIL_MACRO_H

/// If compilers fully support C++17 it can be changed to:<br>
/// #define MAYBE_UNUSED [[maybe_unused]]
#define MAYBE_UNUSED [[gnu::unused]]

#endif  // DLA_INTERFACE_UTIL_MACRO_H

//
// Distributed Linear Algebra Interface (DLAI)
//
// Copyright (c) 2018-2019, ETH Zurich
// All rights reserved.
//
// Please, refer to the LICENSE file in the root directory.
// SPDX-License-Identifier: BSD-3-Clause
//

#ifndef DLA_INTERFACE_DLA_HPX_LINALG_H
#define DLA_INTERFACE_DLA_HPX_LINALG_H

#ifdef DLA_HAVE_HPX_LINALG

#include "hpx_linalg/hpx_linalg.h"

namespace dla_interface { namespace hpx_linalg_wrappers {}}

#endif

#endif  // DLA_INTERFACE_DLA_HPX_LINALG_H
